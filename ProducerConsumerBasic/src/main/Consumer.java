package main;

public class Consumer implements Runnable {

	Interchanger<Character> interchanger;

	Consumer(Interchanger<Character> interchanger) {
		this.interchanger = interchanger;
	}

	@Override
	public void run() {
		try {
			while (true) {
				char value = this.interchanger.get();
				System.out.println("Consumer gets: " + value);
			}
		} catch (InterruptedException e) {

		}

	}

}
