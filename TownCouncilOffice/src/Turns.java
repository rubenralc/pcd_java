public class Turns {
	private int turn;
	private boolean turnWaited;
	private Service service;

	public Turns() {
		this.turn = -1;
		this.turnWaited = false;
		this.service = null;
	}

	public synchronized Service checkTurn(int turn) throws InterruptedException {
		this.turn = turn;
		notifyAll();
		while (!turnWaited)
			wait();
		this.turnWaited = false;
		return this.service;
	}

	public synchronized void waitTurn(int turn, Service service) throws InterruptedException {
		while (turn != this.turn)
			wait();
		this.turnWaited = true;
		this.service = service;
		this.turn = -1;
		notifyAll();
	}
}
