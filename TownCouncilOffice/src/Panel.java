public class Panel {
	private int maxTurns;
	private int actualTurn;
	private int usingTurn;

	public Panel(int maxTurns) {
		this.maxTurns = maxTurns;
		this.actualTurn = 0;
		this.usingTurn = -1;
	}

	public synchronized void inc() {
		actualTurn = (actualTurn + 1) % maxTurns;
	}

	public synchronized int turn() throws InterruptedException {
		/*while (usingTurn == actualTurn)
			wait();
		usingTurn = actualTurn;
		notifyAll();*/

		return actualTurn;
	}
}
