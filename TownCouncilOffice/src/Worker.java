public class Worker implements Runnable {

	private int nbr;
	private Panel panel;
	private Turns turns;
	private Service service;

	public Worker(int nbr, Panel panel, Turns turns, Service service) {
		this.nbr = nbr;
		this.panel = panel;
		this.turns = turns;
		this.service = service;
	}

	@Override
	public void run() {
		try {
			while (true) {
				int turn = panel.turn();
				this.service = turns.checkTurn(turn);
				System.out.println("Worker number " + nbr
						+ " is asking for service '" + service.name
						+ "' at turn " + turn);
				Thread.sleep(2000);
				service.service(turn);
				panel.inc();
			}
		} catch (InterruptedException e) {
		}
	}

}
