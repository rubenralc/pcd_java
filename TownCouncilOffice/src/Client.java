import java.util.Random;

public class Client implements Runnable {

	private int nbr;
	private Tickets tickets;
	private Turns turns;
	private Service[] services;

	public Client(int nbr, Tickets tickets, Turns turns, Service[] services) {
		this.nbr = nbr;
		this.tickets = tickets;
		this.turns = turns;
		this.services = services;
	}

	@Override
	public void run() {
		try {
			while (true) {
				int ticket = tickets.ticket();
				Random rand = new Random();
				Service service = services[rand.nextInt(services.length)];
				turns.waitTurn(ticket, service);
				System.out.println("Client number " + nbr
						+ " is requesting service '" + service.name
						+ "' at turn " + ticket);
				Thread.sleep(2000);
				service.service(ticket);
			}
		} catch (InterruptedException e) {
		}
	}

}
