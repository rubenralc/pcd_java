public class Office {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int option = 2;

		Panel panel = new Panel(3);
		Tickets tickets = new Tickets(3);
		Turns turns = new Turns();
		Service service = new Service(3);
		Service[] services = { service };
		switch (option) {
		case 3:
			services = new Service[2];
			services[0] = new MarriageCertificate(3);
			services[1] = new FineExemption(3);
			new Thread(new Client(1, tickets, turns, services)).start();
			new Thread(new Worker(1, panel, turns, service)).start();
			break;
		case 2:
			new Thread(new Worker(1, panel, turns, service)).start();
			new Thread(new Client(1, tickets, turns, services)).start();
			new Thread(new Worker(2, panel, turns, service)).start();
			new Thread(new Client(2, tickets, turns, services)).start();
			break;
		case 1:
			new Thread(new Worker(1, panel, turns, service)).start();
			new Thread(new Client(1, tickets, turns, services)).start();
			new Thread(new Client(2, tickets, turns, services)).start();
			break;
		case 0:
			new Thread(new Worker(1, panel, turns, service)).start();
			new Thread(new Client(1, tickets, turns, services)).start();
			break;
		}

	}
}
