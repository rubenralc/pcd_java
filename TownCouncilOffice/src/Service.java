public class Service {

	private int[] serviced;
	private boolean[] serviceDone;
	protected String name = "Normal";

	public Service(int maxTurns) {
		this.serviced = new int[maxTurns];
		this.serviceDone = new boolean[maxTurns];
		for (int i = 0; i < maxTurns; i++) {
			this.serviced[i] = 0;
			this.serviceDone[i] = false;
		}
	}

	protected synchronized void service(int turn)
			throws InterruptedException {
		serviced[turn] = serviced[turn] + 1;
		
		while (serviced[turn] < 2)
			wait();
		if (serviceDone[turn])
		{
			serviced[turn] = 0;
			serviceDone[turn] = false;
		}
		else
			serviceDone[turn] = true;
		
		notifyAll();
	}
}
