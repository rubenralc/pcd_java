
public class Tickets {
	private int maxTickets;
	private int actualTickets;
	
	public Tickets (int maxTickets){
		this.maxTickets = maxTickets;
		this.actualTickets = 0;
	}
	
	public synchronized int ticket(){
		int result = actualTickets;
		actualTickets = (actualTickets + 1) % maxTickets;
		return result;
	}
}
