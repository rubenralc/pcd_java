public class Alice extends Thread {
	Room room;
	int counter = 0;
	// alice + the other prisoners
	int prisonersNumber;
	int goodNumber = prisonersNumber + 1;
	boolean free = false;

	public Alice(Room room, int prisonersNumber) {
		this.room = room;
		this.prisonersNumber = prisonersNumber;
	}

	// ============= roomIn ===================

	public void roomIn() {
		synchronized (room) {
			if (room.getState() == "off") {
				room.turnOn();
				room.turnOff();
				System.out.println("Alice turnOn->turnOff the light");

			}
			if (room.getState() == "on" && counter < ((prisonersNumber * 2) - 2)) {
				room.turnOff();
				// System.out.println(room);
				++counter;
				System.out.println("Alice turnOff: " + counter + "times");
			}
			if (counter >= ((prisonersNumber * 2) - 2)) {
				System.out.println("freeAll the " + counter + "prisoners");
				free = true;
			}
			System.out.println("Alice ask Alice if free: " + free);
			// Simulate.HWInterrup();
		}
		Simulate.HWInterrup();
	}

	// ===to communicate freeness to other prisoners and stop processes

	public boolean askForFree() {
		return free;
	}

	// ===================== run ================

	public void run() {

		// for(int i=1; i<100 && !free; ++i) roomIn();

		while (!free) {
			roomIn();
		}
	}

}
