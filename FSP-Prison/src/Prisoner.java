public class Prisoner extends Thread {
	Room room;
	boolean firstTime = true;
	boolean secondTime = true;
	boolean free;
	Alice myAlice;
	// this is the prisoner number
	int i;

	public Prisoner(Room room, Alice alice, int i) {
		this.room = room;
		myAlice = alice;
		this.i = i;
	}

	// === entering the room====

	public void roomIn() {
		synchronized (room) {
			if (firstTime) {
				if (room.getState() == "off") {
					room.turnOn();
					firstTime = false;
					System.out.println("Prisoner " + i
							+ " turnOn the light for the first time");
				} else {
					room.turnOff();
					room.turnOn();
					System.out
							.println("Prisoner "
									+ i
									+ " turnOff->turnOn the light before the first time");
				}
			}
			if (!firstTime) {
				if (secondTime) {
					if (room.getState() == "off") {
						room.turnOn();
						secondTime = false;
						System.out.println("Prisoner " + i
								+ " turnOn the light for the second time");
					} else {
						room.turnOff();
						room.turnOn();
						System.out
								.println("Prisoner "
										+ i
										+ " turnOff->turnOn the light before the second time");
					}
				}
				if (!secondTime) {
					if (room.getState() == "off") {
						room.turnOn();
						room.turnOff();
						System.out
								.println("Prisoner "
										+ i
										+ " turnOn->turnOff the light after the second time");
					} else {
						room.turnOff();
						room.turnOn();
						System.out
								.println("Prisoner "
										+ i
										+ " turnOff->turnOn the light after the second time");
					}
				}
			}
			System.out.println("Prisoner " + i + " ask Alice if free: "
					+ myAlice.askForFree());
			// Simulate.HWInterrup();
		}
		Simulate.HWInterrup();
	}

	// ======= the run ===========

	public void run() {
		// for(int i=1; i<100&& !free; ++i)roomIn();

		while (!myAlice.askForFree()) {
			roomIn();
		}
	}

}
