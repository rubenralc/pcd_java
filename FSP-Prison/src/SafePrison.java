public class SafePrison {

	public static void main(String[] args) {

		// === constructors
		Room room = new Room("off");
		Alice alice = new Alice(room, 50);
		Prisoner[] prisoners = new Prisoner[49];
		for (int i = 1; i < 50; ++i) {
			prisoners[i-1] = new Prisoner(room, alice, i);
		}

		// ==== start threads
		alice.start();
		for (int i = 0; i < 49; ++i) {
			prisoners[i].start();
		}
	}

}
