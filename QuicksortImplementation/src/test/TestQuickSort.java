package test;

import org.junit.Assert;
import org.junit.Test;

import quicksort.Quicksorter;


public class TestQuickSort {

	@Test
	public void testQuicksortOneValue(){
		Quicksorter qs = new Quicksorter();
		
		int[] expecteds = new int[]{1};
		int[] actuals = qs.qsort(new int[]{1}, 0, 1);
		
		Assert.assertArrayEquals(expecteds, actuals);
	}
	
	@Test
	public void testQuicksortTwoValues(){
		Quicksorter qs = new Quicksorter();
		
		int[] expecteds = new int[]{1, 2};
		int[] actuals = qs.qsort(new int[]{2, 1}, 0, 1);
		
		Assert.assertArrayEquals(expecteds, actuals);
	}
}