package test;

import static org.junit.Assert.*;
import org.junit.Test;
import bst.BSTree;

public class TestBinarySearchTree {

	@Test
	public void testCreateEmptyGivesEmpty() {
		BSTree tree = new BSTree();
		tree.create_empty();
		
		boolean expected = true;
		boolean actual = tree.is_empty();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void testInsertGivesNotEmpty(){
		BSTree tree = new BSTree();
		tree.create_empty();
		
		tree.insert(2.345);
		
		boolean expected = false;
		boolean actual = tree.is_empty();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void testInordTransverseAddingOne(){
		BSTree tree = new BSTree();
		tree.create_empty();
		
		tree.insert(2.345);
		
		String expected = "2.345";
		String actual = tree.inord_transverse();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void testInordTransverseAddingTwo(){
		BSTree tree = new BSTree();
		tree.create_empty();
		
		tree.insert(2.345);
		tree.insert(4.5622);
		
		String expected = "2.345 - 4.5622";
		String actual = tree.inord_transverse();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void testInordTransverseAddingThree(){
		BSTree tree = new BSTree();
		tree.create_empty();
		
		tree.insert(2.345);
		tree.insert(4.5622);
		tree.insert(1.456);
		
		String expected = "1.456 - 2.345 - 4.5622";
		String actual = tree.inord_transverse();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void testInordTransverseAddingFour(){
		BSTree tree = new BSTree();
		tree.create_empty();
		
		tree.insert(2.345);
		tree.insert(4.5622);
		tree.insert(1.456);
		tree.insert(0);
		
		String expected = "0.0 - 1.456 - 2.345 - 4.5622";
		String actual = tree.inord_transverse();
		
		assertEquals(expected, actual);
		
		tree.create_empty();
		
		tree.insert(2.345);
		tree.insert(4.5622);
		tree.insert(1.456);
		tree.insert(5);
		
		expected = "1.456 - 2.345 - 4.5622 - 5.0";
		actual = tree.inord_transverse();
		
		assertEquals(expected, actual);
	}
}
