package main;

public class Writer implements Runnable {

	private char toWrite;

	Writer(char toWrite) {
		this.toWrite = toWrite;
	}

	@Override
	public void run() {
		for (int i = 0; i < 100; ++i) {
			System.out.print(toWrite);
		}
	}

}
