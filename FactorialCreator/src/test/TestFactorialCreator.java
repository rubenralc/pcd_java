package test;

import org.junit.Assert;
import org.junit.Test;

import factorial.FactorialGenerator;


public class TestFactorialCreator {

	@Test
	public void testFactorialOfZeroAndOneGivesOne(){
		FactorialGenerator fg = new FactorialGenerator();
		int expected = 1;
		int actual = fg.getFactorial(0);
	
		Assert.assertEquals(expected, actual);
		
		expected = 1;
		actual = fg.getFactorial(1);
		
		Assert.assertEquals(expected, actual);
	}
	
	
	@Test
	public void testFactorialOfTwoGivesTwo(){
		FactorialGenerator fg = new FactorialGenerator();
		int expected = 2;
		int actual = fg.getFactorial(2);
		
		Assert.assertEquals(expected, actual);
	}
	
	
	@Test
	public void testFactorialOfThreeGivesSix(){
		FactorialGenerator fg = new FactorialGenerator();
		int expected = 6;
		int actual = fg.getFactorial(3);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testFactorialForMultipleKindOfNumbers(){
		FactorialGenerator fg = new FactorialGenerator();
		int expected = 24;
		int actual = fg.getFactorial(4);
		
		Assert.assertEquals(expected, actual);

		expected = 120;
		actual = fg.getFactorial(5);
		
		Assert.assertEquals(expected, actual);

		expected = 720;
		actual = fg.getFactorial(6);
		
		Assert.assertEquals(expected, actual);

		expected = 5040;
		actual = fg.getFactorial(7);
		
		Assert.assertEquals(expected, actual);

		expected = 40320;
		actual = fg.getFactorial(8);
		
		Assert.assertEquals(expected, actual);

		expected = 362880;
		actual = fg.getFactorial(9);
		
		Assert.assertEquals(expected, actual);

		expected = 3628800;
		actual = fg.getFactorial(10);
		
		Assert.assertEquals(expected, actual);
	}
}