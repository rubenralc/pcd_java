package factorial;

public class FactorialGenerator {
	public int getFactorial(int number) {
		int result = 1;
		
		for(int i = number; i>0; --i){
			result *= i;
		}
		
		return result;
	}
}
