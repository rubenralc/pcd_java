public class RingProcess implements Runnable {

	private Token token;
	private int processNumber;
	private int nextProcessNumber;

	public RingProcess(Token token, int processNumber) {
		this.token = token;
		this.processNumber = processNumber;
		this.nextProcessNumber = processNumber + 1 < token.getProcessesTotal() ? processNumber + 1
				: 0;
	}

	@Override
	public void run() {
		try {
			while (true) {
				token.read(processNumber);

				System.out.println("The process with number " + processNumber
						+ " has got the hang");

				token.write(nextProcessNumber);
			}

		} catch (InterruptedException e) {

		}

	}

}
