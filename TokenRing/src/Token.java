
public class Token {
	
	private boolean[] var;
	
	public Token(int processes){
		this.var = new boolean[processes];
		for(int i = 0; i < processes; ++i){
			this.var[i] = false;
		}
		this.var[0] = true;
	}
	
	public synchronized void read(int process) throws InterruptedException {
		while(!var[process])
			wait();
		var[process] = false;
		notifyAll();
	}
	
	public synchronized void write(int process) throws InterruptedException {
		while(var[process])
			wait();
		var[process] = true;
		notifyAll();
	}
	
	public int getProcessesTotal(){
		return this.var.length;
	}
}
