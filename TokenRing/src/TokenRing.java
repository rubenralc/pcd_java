
public class TokenRing {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Token token = new Token(3);
		
		new Thread(new RingProcess(token, 0)).start();
		new Thread(new RingProcess(token, 1)).start();
		new Thread(new RingProcess(token, 2)).start();
	}

}
