package main;

public class ProdCon {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Interchanger<Character> interchangerChar = new Interchanger<Character>();
		new Thread(new Producer<Character>(interchangerChar, 'a')).start();
		new Thread(new Consumer<Character>(interchangerChar)).start();

		Interchanger<String> interchangerString = new Interchanger<String>();
		new Thread(new Producer<String>(interchangerString, "test")).start();
		new Thread(new Consumer<String>(interchangerString)).start();

		Interchanger<Integer> interchangerInteger = new Interchanger<Integer>();
		new Thread(new Producer<Integer>(interchangerInteger, 4)).start();
		new Thread(new Consumer<Integer>(interchangerInteger)).start();
	}

}
