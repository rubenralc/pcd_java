package main;

import java.util.Stack;

public class Interchanger<E> {
	private Stack<E> stacker;
	private final int MAX = 4;

	public Interchanger() {
		this.stacker = new Stack<E>();
	}

	public synchronized void put(E o) throws InterruptedException {
		while (this.stacker.size() == this.MAX)
			wait();
		this.stacker.push(o);
		notifyAll();
	}

	public synchronized E get() throws InterruptedException {
		while (this.stacker.empty())
			wait();
		E item = this.stacker.pop();
		notifyAll();
		return item;
	}

	@Override
	public String toString() {
		String result = "";

		for (int i = 0; i < this.stacker.size(); ++i) {
			result += "Element " + i + ": " + this.stacker.elementAt(i);
		}

		return result;
	}
}
