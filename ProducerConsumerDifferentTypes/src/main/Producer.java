package main;

public class Producer<E> implements Runnable {

	Interchanger<E> interchanger;
	E toPut;

	Producer(Interchanger<E> interchanger, E toPut) {
		this.interchanger = interchanger;
		this.toPut = toPut;
	}

	@Override
	public void run() {
		try {
			while (true) {
				System.out.println("Producers put: " + this.toPut.toString());
				this.interchanger.put(this.toPut);
			}
		} catch (InterruptedException e) {

		}

	}

}
