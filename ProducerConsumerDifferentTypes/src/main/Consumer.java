package main;

public class Consumer<E> implements Runnable {

	Interchanger<E> interchanger;

	Consumer(Interchanger<E> interchanger) {
		this.interchanger = interchanger;
	}

	@Override
	public void run() {
		try {
			while (true) {
				E value = this.interchanger.get();
				System.out.println("Consumer gets: " + value.toString());
			}
		} catch (InterruptedException e) {

		}

	}

}
