package main;

public class Writer implements Runnable {

	private char toWrite;
	private char toWait;
	private String written;

	Writer(char toWrite, char toWait, String written) {
		this.toWrite = toWrite;
		this.toWait = toWait;
		this.written = written;
	}

	@Override
	public synchronized void run() {
		try {
			for (int i = 0; i < 100; ++i) {
				while (!written.endsWith(String.valueOf(toWait)))
					wait();
				written = String.valueOf(toWrite);
				System.out.print(toWrite);
				notifyAll();
			}
		} catch (InterruptedException e) {

		}
	}

}
