package main;

public class Producer implements Runnable {

	Interchanger<Character> interchanger;
	String alphabet = "abcdefghijklmnopqrstuvwxyz";

	Producer(Interchanger<Character> interchanger) {
		this.interchanger = interchanger;
	}

	@Override
	public void run() {
		try {
			int ai = 0;
			while (true) {
				System.out.println("Producers put: " + alphabet.charAt(ai));
				this.interchanger.put(alphabet.charAt(ai));
				ai = (ai + 1) % alphabet.length();
			}
		} catch (InterruptedException e) {

		}

	}

}
