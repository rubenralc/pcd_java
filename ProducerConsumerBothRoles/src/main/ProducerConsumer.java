package main;

public class ProducerConsumer implements Runnable {

	Interchanger<Character> interchanger;
	String alphabet = "abcdefghijklmnopqrstuvwxyz";

	ProducerConsumer(Interchanger<Character> interchanger) {
		this.interchanger = interchanger;
	}

	@Override
	public void run() {
		try {
			int ai = 0;
			while (true) {
				Character item = this.interchanger.put_get(alphabet.charAt(ai));
				if(item == null)
					System.out.println("ProducerConsumer puts: " + alphabet.charAt(ai));
				else
					System.out.println("ProducerConsumer gets: " + item);
				ai = (ai + 1) % alphabet.length();
			}
		} catch (InterruptedException e) {

		}

	}

}
