package main;

public class ProdCon {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Interchanger<Character> interchanger = new Interchanger<Character>();
		new Thread(new Producer(interchanger)).start();
		new Thread(new Consumer(interchanger)).start();
		new Thread(new ProducerConsumer(interchanger)).start();
	}

}
