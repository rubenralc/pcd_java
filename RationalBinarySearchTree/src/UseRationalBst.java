import bst.BSTree;
import numbers.Rational;

public class UseRationalBst {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		System.out
				.println("Please enter a sequence of rational numbers separated by ';'");
		int ch;
		StringBuffer sb = new StringBuffer();
		while ((ch = System.in.read()) != 10) {
			sb.append((char) ch);
		}
		String[] input = sb.toString().split(";");

		BSTree tree = new BSTree();
		tree.create_empty();
		
		for (int i = 0; i < input.length; ++i) {
			String[] rational = input[i].split("/");
			Rational rat = null;
			if(rational.length==2)
				rat = new Rational(Integer.parseInt(rational[0]), Integer.parseInt(rational[1]));
			
			if(rational.length == 1)
				rat = new Rational(Integer.parseInt(rational[0]), 1);
			
			if(rat != null)
				tree.insert(rat);
		}
		
		System.out.println(tree.inord_transverse());
	}

}
