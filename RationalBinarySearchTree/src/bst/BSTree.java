package bst;

import numbers.Rational;

public class BSTree {
	private Node root;

	public void create_empty() {
		this.root = null;
	}

	public boolean is_empty() {
		return root == null;
	}

	public void insert(Rational value) {
		if (this.root == null)
			this.root = new Node(value);
		else
			this.root.add_new(value);
	}

	public String inord_transverse() {
		return this.root.print_inord();
	}
}
