package bst;

import numbers.Rational;

public class Node {
	private Rational value;

	private Node less, more;

	public Node(Rational value) {
		this.value = value;
	}

	public void add_new(Rational value) {
		if (this.value.compareTo(value) >= 0) {
			if (this.less == null)
				this.less = new Node(value);
			else
				this.less.add_new(value);
		} else {
			if (this.more == null)
				this.more = new Node(value);
			else
				this.more.add_new(value);
		}
	}

	public String print_inord() {
		String result = new String();

		if (this.less != null)
			result += this.less.print_inord() + " - ";

		result += this.toString();

		if (this.more != null)
			result += " - " + this.more.print_inord();

		return result;
	}

	@Override
	public String toString() {
		return this.value.toString();
	}
}
