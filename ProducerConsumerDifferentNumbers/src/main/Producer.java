package main;

public class Producer implements Runnable {

	Interchanger interchanger;
	String alphabet = "abcdefghijklmnopqrstuvwxyz";
	int quantity;

	Producer(Interchanger interchanger, int quantity) {
		this.interchanger = interchanger;
		this.quantity = quantity;
	}

	@Override
	public void run() {
		try {
			int ai = 0;
			while (true) {
				String putted = "";
				for(int i = 0; i < quantity; ++i){
					putted += alphabet.charAt(ai);
				}
				System.out.println("Producers put: " + putted);
				this.interchanger.put(alphabet.charAt(ai), quantity);
				ai = (ai + 1) % alphabet.length();
			}
		} catch (InterruptedException e) {

		}

	}

}
