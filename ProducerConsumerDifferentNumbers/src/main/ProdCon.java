package main;

public class ProdCon {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Interchanger interchanger = new Interchanger();
		new Thread(new Producer(interchanger, 3)).start();
		new Thread(new Consumer(interchanger, 3)).start();
		new Thread(new Producer(interchanger, 2)).start();
		new Thread(new Consumer(interchanger, 2)).start();
		new Thread(new Producer(interchanger, 1)).start();
		new Thread(new Consumer(interchanger, 1)).start();
		
	}

}
