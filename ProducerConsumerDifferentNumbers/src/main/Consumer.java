package main;

public class Consumer implements Runnable {

	Interchanger interchanger;
	int quantity;

	Consumer(Interchanger interchanger, int quantity) {
		this.interchanger = interchanger;
		this.quantity = quantity;
	}

	@Override
	public void run() {
		try {
			while (true) {
				Character[] value = this.interchanger.get(quantity);
				String gotten = "";
				for (int i = 0; i < value.length; ++i) {
					gotten += value[i];
				}
				System.out.println("Consumer gets: " + gotten);
			}
		} catch (InterruptedException e) {

		}

	}

}
