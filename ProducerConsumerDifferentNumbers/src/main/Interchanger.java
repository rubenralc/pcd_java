package main;

import java.util.Stack;

public class Interchanger {
	private Stack<Character> stacker;
	private final int MAX = 4;

	public Interchanger() {
		this.stacker = new Stack<Character>();
	}

	public synchronized void put(Character o, int nbr)
			throws InterruptedException {
		while (this.stacker.size() + nbr >= this.MAX)
			wait();
		for (int i = 0; i < nbr; ++i) {
			this.stacker.push(o);
		}
		notifyAll();
	}

	public synchronized Character[] get(int nbr) throws InterruptedException {
		while (this.stacker.size() - nbr <= 0)
			wait();
		Character[] items = new Character[nbr];
		for (int i = 0; i < nbr; ++i) {
			items[i] = this.stacker.pop();
		}
		notifyAll();
		return items;
	}

	@Override
	public String toString() {
		String result = "";

		for (int i = 0; i < this.stacker.size(); ++i) {
			result += "Element " + i + ": " + this.stacker.elementAt(i);
		}

		return result;
	}
}
